## 介绍
网市场云建站系统-给别人部署，就是部署的这个，wangmarket本身以jar形式存在，以便升级

# 线上部署
### 1. 服务器环境

##### 1.1 服务器系统环境
CentOS 7.4（7.0~7.6应该都行，如果有7.4，请选7.4，其他版本只是猜测应该没问题）

##### 1.2 jdk8及tomcat8.5 安装
https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3690921&doc_id=1101390  

##### 1.3 mysql 5.7 安装
安装：https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3690920&doc_id=1101390  
 **请注意版本，必须5.7！必须！别用8.0啥的**  

### 2. 部署应用

##### 2.1 mysql数据库文件
下载：https://gitee.com/mail_osc/wangmarket/raw/master/else/wangmarket.sql  

##### 2.2 部署网市场应用
上传网市场应用到服务器 /mnt/tomcat8/webapps/ROOT/ 下  

方式一（建议）：直接执行命令下载安装，适用于快速安装使用，不考虑二次开发情况
````
cd /mnt/tomcat8/webapps/ROOT/
rm -rf ./*
wget http://down.zvo.cn/wangmarket/wangmarket.zip
unzip wangmarket.zip
````

方式二：本地编译，适用于二次开发    
你可以直接将本项目拉到你本地eclipse中进行编译运行，eclipse不知如何导入，可参考 https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3600882&doc_id=1101390  
将生成的运行文件上传本项目到 /mnt/tomcat8/webapps/ROOT/ 下 


### 3. 配置数据库连接
参考： https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3634642&doc_id=1101390

### 4. 启动，使用
启动tomcat，启动命令为:  

````
cd /mnt/tomcat8/bin/
./startup.sh
````

启动可能需要半分钟，启动成功后，访问 /login.do 按照安装步骤提示及说明进行操作

### 5. 版本更新
直接更新 WEB-INF/lib/wangmarket-xxx.jar 即可完成更新 (有时还会更新其他jar包，到时详细查阅更新说明里需要更新的jar包即可。)

# 二次开发及扩展
### 原本的页面及功能修改、重写
比如，感觉原本的登录页面不好看，那么可以对登录页进行重写。重写方式参考： https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3631374&doc_id=1101390  
wangmarket 的源码文件在项目：  https://gitee.com/mail_osc/wangmarket  

### 定制开发自己想要的功能
参考：  https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3213258&doc_id=1101390